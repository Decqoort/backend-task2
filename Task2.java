import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in);
        System.out.print("Температура: ");
        int temperature = myScanner.nextInt();
        System.out.print("Дождь/снег (y/n): ");
        boolean precipitation = myScanner.next().charAt(0) == 'y';
        System.out.print("Скорость ветра: ");
        int wind = myScanner.nextInt();

        if (temperature >= 0) {
            if (precipitation && wind < 15) {
                System.out.println("На улице дождь. Стоит взять зонтик.");
            } else if (precipitation){
                System.out.println("На улице дождь и сильный ветер. Стоит надеть дождевик.");
            } else if (wind < 15) {
                if (temperature < 15) {
                    System.out.println("Можете идти гулять, но стоит надеть что-то теплое.");
                } else if (temperature > 30) {
                    System.out.println("На улице жара, лучше включить кондиционер.");
                } else {
                    System.out.println("Сегодня тепло, время прогуляться.");
                }
            } else {
                System.out.println("Будьте осторожны, на улице сильный ветер.");
            }
        } else {
            if (wind >= 15) {
                System.out.println("На улице метель, лучше остаться дома.");
            } else if (temperature >= -20) {
                System.out.println("Оденьтесь теплее.");
            } else if (temperature > -30) {
                System.out.println("Если хотите погулять, стоит надеть самые теплые вещи.");
            } else {
                System.out.println("На улице дубак, лучше остаться дома и попить теплого чаю.");
            }
        }
    }
}
